package cn.mxleader.quickdoc.entities

data class TfMatch (val label: String, var probability: Float)