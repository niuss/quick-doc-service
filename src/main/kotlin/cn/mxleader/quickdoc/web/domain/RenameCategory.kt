package cn.mxleader.quickdoc.web.domain

data class RenameCategory(var oldType: String, var newType: String)